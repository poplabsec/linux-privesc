


function rfs_banner() {
  cat << EOF
     .------.
    /  .-.  \
   |  | | |  |
   | (___) |  |
    \  .--.  /
     '--''--'
EOF
}




function net_info() {
  # Get all network interfaces
  interfaces=( $(ifconfig | grep -w name | awk '{print $1}') )

  # Loop through each interface
  for iface in "${interfaces[@]}"; do
    # Get IP addresses (IPv4 only)
    ip_addr=$(ifconfig $iface | grep "inet addr:" | awk '{print $2}')

    # Get MAC address
    mac_addr=$(ifconfig $iface | grep "HWaddr" | awk '{print $5}')

    # Skip loopback interface
    if [ "$iface" != "lo" ]; then
      # Get default gateway (assuming first route is default)
      gateway=$(ip route show scope global | grep default via | head -n 1 | awk '{print $3}')

      # Print information for current interface
      echo "Interface: $iface"
      echo "  IP Address: $ip_addr"
      echo "  MAC Address: $mac_addr"
      echo "  Default Gateway: $gateway"
      echo ""  # Add a blank line between interfaces
    fi
  done
}


function sys_info() {
  # Get OS Name and Version
  os_name=$(cat /etc/os-release | grep -w PRETTY_NAME | cut -d '=' -f2 | tr -d '"')

  # Get Kernel Version
  kernel_version=$(uname -r)

  # Get CPU Information
  cpu_info=$(cat /proc/cpuinfo | grep "model name" | head -n 1 | awk '{print $4}')
  cpu_cores=$(grep "cpu cores" /proc/cpuinfo | wc -l)

  # Get Memory Information
  total_mem=$(grep MemTotal /proc/meminfo | awk '{print $2}')
  free_mem=$(grep MemFree /proc/meminfo | awk '{print $2}')

  # Get Disk Usage (Root Partition)
  df / | grep "/" | awk '{print $5}' | cut -d "%" -f1

  # Print System Information
  echo "Operating System: $os_name"
  echo "Kernel Version: $kernel_version"
  echo "CPU: $cpu_info x $cpu_cores Cores"
  echo "Total Memory: $total_mem kB"  # Converted to kilobytes
  echo "Free Memory: $free_mem kB"    # Converted to kilobytes
  echo "Disk Usage (/): $(df / | grep "/" | awk '{print $5}' | cut -d "%" -f1)%"
}


function check_sticky_bit() {
  # Find all files (excluding directories) with the sticky bit set
  find . ! -type d -perm /1000 | while read file; do
    # Get file permissions using stat
    stat_output=$(stat -c "%a" "$file")

    # Check user sticky bit (1000) and group sticky bit (2000)
    if [[ $stat_output =~ [12]000 ]]; then
      # Extract user/group name using id command
      owner=$(stat -c "%u" "$file")
      group=$(stat -c "%g" "$file")
      user_name=$(id -n -u "$owner")
      group_name=$(id -n -g "$group")

      # Print file path and sticky bit owner
      if [[ $stat_output =~ 1000 ]]; then
        echo "File: $file (Sticky bit for user: $user_name)"
      else
        echo "File: $file (Sticky bit for group: $group_name)"
      fi
    fi
  done
}


function find_potential_non_default_scripts() {
  # Exclude common system directories
  exclude_dirs=(/bin /sbin /usr/bin /usr/sbin)

  # Find all executable files (excluding directories)
  find / -type f -perm /111 ! -path "${exclude_dirs[@]}" -print | while read file; do
    # Check file modification time (heuristic)
    file_age=$(stat -c %y "$file" | awk '{ print $1 }')
    days_old=$(( ( $(date +%s) - $file_age ) / (60 * 60 * 24) ))

    # Check if file is owned by root (heuristic)
    file_owner=$(stat -c %U "$file" | awk '{ print $1 }')

    # Print information about potential non-default scripts
    if [[ $days_old -lt 365 && $file_owner == "root" ]]; then
      echo "File: $file (Potentially non-default, Recently added, Owned by root)"
    fi
  done
}


function check_non_default_hosts() {
  # Define default entries (localhost and 127.0.0.1 loopback)
  default_entries=("127.0.0.1 localhost" "::1 localhost")

  # Read /etc/hosts file line by line
  while IFS= read -r line; do
    # Skip comments and empty lines
    if [[ -z "$line" || $line =~ ^# ]]; then
      continue
    fi

    # Extract IP address and hostname
    ip_addr=$(echo "$line" | awk '{print $1}')
    hostnames=$(echo "$line" | awk '{print $2}')

    # Check if IP or hostname matches default entries
    is_default=false
    for entry in "${default_entries[@]}"; do
      if [[ "$line" == "$entry" || "$hostnames" =~ $entry ]]; then
        is_default=true
        break
      fi
    done

    # Print non-default entries
    if ! $is_default; then
      echo "Non-default entry: $line"
    fi
  done < /etc/hosts
}


function check_potential_sudo_vulnerabilities() {
  # Check sudo version
  sudo_version=$(sudo -V | grep "sudo ver" | awk '{print $3}')

  # Check for known vulnerable versions (replace with a reliable source)
  # This is a placeholder and might not include all vulnerable versions
  vulnerable_versions=( "1.5.0" "1.5.1" "1.5.2" "1.5.3" "1.5.4" "1.5.5" )

  # Check if version matches a vulnerable version
  for version in "${vulnerable_versions[@]}"; do
    if [[ "$sudo_version" == "$version" ]]; then
      echo "Warning: Sudo version $sudo_version might be vulnerable."
      return 1  # Exit function with non-zero code to indicate a potential issue
    fi
  done

  # Check for world-writable sudoers file (not recommended)
  if [[ -w /etc/sudoers && $(stat -c %A /etc/sudoers | grep w) ]]; then
    echo "Warning: /etc/sudoers file has world write permissions (not recommended)."
  fi

  echo "No potential sudo vulnerabilities detected (basic checks)."
}

# Example usage (assuming sudo doesn't require a password)
check_potential_sudo_vulnerabilities
if [[ $? -eq 1 ]]; then
  echo "Further investigation recommended for potential sudo vulnerabilities."
fi


function check_kernel_version() {
  # Get kernel version
  kernel_version=$(uname -r)
  echo "Kernel Version: $kernel_version"
}


function check_exploit_suggestions() {
  # Get kernel version
  kernel_version=$(uname -r)

  # Search online database (replace with a reliable source)
  # This is for demonstration purposes only and might not be reliable
  search_url="https://www.exploit-db.com/search?q=Linux+Kernel+$kernel_version"
  echo "**Disclaimer:** This search might not be reliable. Verify information from official sources."
  echo "Potential exploit suggestions (for informational purposes only):"
  wget -qO- "$search_url" | grep -Eo "(<a[^>]+href=\"\/exploits\/\d+\/\S+\">)([^<]+)(<\/a>)" | head -n 5
}


function check_user_info() {
  # Get all system users (including service accounts)
  all_users=$(cut -d: -f1 /etc/passwd)

  # Get all local users (with home directories)
  local_users=$(getent passwd | cut -d: -f1)

  # Get currently logged-in users (excluding processes)
  logged_in_users=$(who | awk '{print $1}')

  # Get last logged-in users (excluding current session and reboot)
  last_logged_users=$(last -w | grep still | awk '{print $1}')

  # Print user information
  echo "** All System Users:**"
  printf "\t%s\n" "${all_users[@]}"  # Print each user in a new line

  echo "** Local Users (with home directories):**"
  printf "\t%s\n" "${local_users[@]}"  # Print each user in a new line

  echo "** Currently Logged-in Users:**"
  printf "\t%s\n" "${logged_in_users[@]}"  # Print each user in a new line

  echo "** Last Logged-in Users (excluding current session and reboot):**"
  printf "\t%s\n" "${last_logged_users[@]}"  # Print each user in a new line
}


function find_history_files() {
  # Get the shell type (e.g., bash, zsh)
  shell_type=$(echo "$SHELL" | cut -d '/' -f 2)

  case "$shell_type" in
    bash)
      history_file="$HOME/.bash_history"
      echo "Bash history file: $history_file"
      ;;
    zsh)
      history_file="$HOME/.zsh_history"
      echo "Zsh history file: $history_file"
      ;;
    *)
      echo "Warning: Shell history file location might differ for '$shell_type' shell."
      ;;
  esac
}


find /proc -name cmdline -exec cat {} \; 2>/dev/null | tr " " "\n"



